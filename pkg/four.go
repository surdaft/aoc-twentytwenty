package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Passport struct {
	BirthYear  *int
	IssueYear  *int
	ExpiryYear *int
	Height     *int
	HeightUnit *string
	HairColour *string
	EyeColour  *string
	PassportId *string
}

func main() {
	var log *logrus.Logger = logrus.New()

	// flag to provide input source
	var inputFile string = ""

	flag.StringVar(&inputFile, "input", "", "Input for task")
	flag.Parse()

	if inputFile == "" {
		log.Error("Filename must not be empty")
		return
	}

	// open file
	dat, err := os.Open(inputFile)
	if err != nil {
		log.Error(err)
		return
	}

	var rows []string

	scanner := bufio.NewScanner(dat)
	for scanner.Scan() {
		rows = append(rows, scanner.Text())
	}

	log.Print("=== Part 1 ===")

	// split the input into its passports on empty lines
	// find all values within the passport group
	// now check all items are found
	// if all items found increment valid

	var groups []Passport
	group := make(map[string]string)
	for _, row := range rows {
		if row == "" {
			passport := Passport{}

			if group["byr"] != "" {
				byr, _ := strconv.Atoi(group["byr"])
				passport.BirthYear = &byr
			}

			if group["iyr"] != "" {
				iyr, _ := strconv.Atoi(group["iyr"])
				passport.IssueYear = &iyr
			}

			if group["eyr"] != "" {
				eyr, _ := strconv.Atoi(group["eyr"])
				passport.ExpiryYear = &eyr
			}

			if group["hgt"] != "" {
				r, _ := regexp.Compile("([0-9]+)(cm|in|)")

				matches := r.FindStringSubmatch(group["hgt"])
				intHeight, _ := strconv.Atoi(matches[1])

				passport.Height = &intHeight
				passport.HeightUnit = &matches[2]
			}

			if group["ecl"] != "" {
				ecl := group["ecl"]
				passport.EyeColour = &ecl
			}

			if group["hcl"] != "" {
				hcl := group["hcl"]
				passport.HairColour = &hcl
			}

			if group["pid"] != "" {
				pid := group["pid"]
				passport.PassportId = &pid
			}

			groups = append(groups, passport)

			group = map[string]string{}
			continue
		}

		parts := strings.Split(row, " ")
		for _, p := range parts {
			pp := strings.Split(p, ":")
			group[string(pp[0])] = string(pp[1])
		}
	}

	var validPassports int = 0
	for _, passport := range groups {
		if hasRequiredFields(passport) {
			validPassports++
		}
	}

	log.Printf("validPassports=%d", validPassports)

	log.Print("=== Part 2 ===")

	validPassports = 0

	for _, passport := range groups {
		if !hasRequiredFields(passport) {
			continue
		}

		if !validateBirthYear(passport.BirthYear) {
			continue
		}

		if !validateExpirationYear(passport.ExpiryYear) {
			continue
		}

		if !validateIssueYear(passport.IssueYear) {
			continue
		}

		if !validateEyeColour(passport.EyeColour) {
			continue
		}

		if !validateHairColour(passport.HairColour) {
			continue
		}

		if !validateHeight(passport.Height, passport.HeightUnit) {
			continue
		}

		if !validatePassportId(passport.PassportId) {
			continue
		}

		validPassports++
	}

	log.Printf("validPassports=%d", validPassports)
}

func hasRequiredFields(passport Passport) bool {
	if passport.BirthYear == nil {
		return false
	}

	if passport.IssueYear == nil {
		return false
	}

	if passport.ExpiryYear == nil {
		return false
	}

	if passport.Height == nil {
		return false
	}

	if passport.HairColour == nil {
		return false
	}

	if passport.EyeColour == nil {
		return false
	}

	if passport.PassportId == nil {
		return false
	}

	return true
}

/**
BirthYear (Birth Year) - four digits; at least 1920 and at most 2002.
IssueYear (Issue Year) - four digits; at least 2010 and at most 2020.
ExpiryYear (Expiration Year) - four digits; at least 2020 and at most 2030.
Height (Height) - a number followed by either cm or in:

    If cm, the number must be at least 150 and at most 193.
    If in, the number must be at least 59 and at most 76.

HairColour (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
EyeColour (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
PassportId (Passport ID) - a nine-digit number, including leading zeroes.
 */
func validateBirthYear(year *int) bool {
	val := *year >= 1920 && *year <= 2002
	//fmt.Printf("%d >= 1920 && %d <= 2002 = %v\n", *year, *year, val)
	return val
}

func validateIssueYear(year *int) bool {
	val := *year >= 2010 && *year <= 2020
	//fmt.Printf("%d >= 2010 && %d <= 2020 = %v\n", *year, *year, val)
	return val
}

func validateExpirationYear(year *int) bool {
	val := *year >= 2020 && *year <= 2030
	//fmt.Printf("%d >= 2020 && %d <= 2030 = %v\n", *year, *year, val)
	return val
}

func validateHairColour(colour *string) bool {
	r, _ := regexp.Compile("^#[0-9A-Fa-f]{6}$")
	val := r.MatchString(*colour)
	fmt.Printf("%s = %v\n", *colour, val)
	return val
}

func validatePassportId(passportId *string) bool {
	r, _ := regexp.Compile("^[0-9]{9}$")
	val := r.MatchString(*passportId)
	fmt.Printf("%s = %v\n", *passportId, val)
	return val
}

func validateEyeColour(colour *string) bool {
	validEyeColours := []string{
		"amb",
		"blu",
		"brn",
		"gry",
		"grn",
		"hzl",
		"oth",
	}

	for _, v := range validEyeColours {
		if v == *colour {
			fmt.Printf("%s = true\n", *colour)
			return true
		}
	}

	fmt.Printf("%s = false\n", *colour)
	return false
}

func validateHeight(height *int, unit *string) bool {
	fmt.Printf("height=%d unit=%s\n", *height, *unit)
	if *unit == "cm" {
		if *height >= 150 && *height <= 193 {
			fmt.Printf("%d >= 150 && %d <= 193 = true\n", *height, *height)
			return true
		}
	}

	if *unit == "in" {
		if *height >= 59 && *height <= 76 {
			fmt.Printf("%d >= 59 && %d <= 76 = true\n", *height, *height)
			return true
		}
	}

	return false
}