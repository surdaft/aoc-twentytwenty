package main

import (
	"bufio"
	"flag"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
)

func main() {
	var log *logrus.Logger = logrus.New()

	// flag to provide input source
	var inputFile string = ""

	flag.StringVar(&inputFile, "input", "", "Input for task")
	flag.Parse()

	if inputFile == "" {
		log.Error("Filename must not be empty")
		return
	}

	// open file
	dat, err := os.Open(inputFile)
	if err != nil {
		log.Error(err)
		return
	}

	var viablePasswords int = 0
	var passwords []string

	scanner := bufio.NewScanner(dat)
	for scanner.Scan() {
		passwords = append(passwords, scanner.Text())
	}

	log.Printf("=== Part 1 ===")
	for _, phrase := range passwords {
		// 1-2 b: abcde
		// 1-2 counts of b
		// in abcde

		parts := strings.Split(phrase, " ")

		bounds := strings.Split(parts[0], "-")

		lowerBound, _ := strconv.Atoi(bounds[0])
		upperBound, _ := strconv.Atoi(bounds[1])

		letterToFind := strings.ReplaceAll(parts[1], ":", "")

		password := parts[2]

		// remove all the characters we are trying to find
		// lets see if its within the bounds
		replacedPassword := strings.ReplaceAll(password, letterToFind, "")
		removedCharacters := len(password) - len(replacedPassword)
		viable := removedCharacters >= lowerBound && removedCharacters <= upperBound

		if viable {
			viablePasswords++
		}

		log.Printf(
			"lowerBound=%d upperBound=%d letterToFind=%s password=%s length=%d viable=%v",
			lowerBound,
			upperBound,
			letterToFind,
			password,
			removedCharacters,
			viable)
	}

	log.Printf("viable passwords - count=%d", viablePasswords)

	log.Printf("=== Part 2 ===")

	viablePasswords = 0

	for _, phrase := range passwords {
		parts := strings.Split(phrase, " ")

		bounds := strings.Split(parts[0], "-")

		firstPosition, _ := strconv.Atoi(bounds[0])
		secondPosition, _ := strconv.Atoi(bounds[1])

		// move 1 to 0 for 0indexing
		firstPosition--
		secondPosition--

		letterToFind := strings.ReplaceAll(parts[1], ":", "")

		password := parts[2]

		var viable bool = false

		// lets match the positions
		// only 1 of them should be matching, both is not viable
		if string(password[firstPosition]) == letterToFind && string(password[secondPosition]) != letterToFind {
			viable = true
		}

		if string(password[firstPosition]) != letterToFind && string(password[secondPosition]) == letterToFind {
			viable = true
		}

		if viable {
			viablePasswords++
		}

		log.Printf(
			"firstPosition=%d secondPosition=%d letterToFind=%s password=%s viable=%v",
			firstPosition,
			secondPosition,
			letterToFind,
			password,
			viable)
	}

	log.Printf("viable passwords - count=%d", viablePasswords)
}
