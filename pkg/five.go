package main

import (
	"bufio"
	"flag"
	"github.com/sirupsen/logrus"
	"math"
	"os"
)

func main() {
	var log *logrus.Logger = logrus.New()

	// flag to provide input source
	var inputFile string = ""

	flag.StringVar(&inputFile, "input", "", "Input for task")
	flag.Parse()

	if inputFile == "" {
		log.Error("Filename must not be empty")
		return
	}

	// open file
	dat, err := os.Open(inputFile)
	if err != nil {
		log.Error(err)
		return
	}

	var rows []string

	scanner := bufio.NewScanner(dat)
	for scanner.Scan() {
		rows = append(rows, scanner.Text())
	}

	log.Print("=== Part 1 ===")

	// 128 rows  -    F means 64 -> 127    -    B means 0 -> 63
	// 8 columns -    L means 0  -> 3      -    R means 4 -> 7

	// zero indexed default rows
	var maxRows int = 127
	var maxCols int = 7

	var largestSeatID float64 = 0
	var smallestSeatID float64 = math.Inf(maxRows * maxCols)

	for _,pass := range rows {
		var seatID float64

		// zero indexed default rows
		var rows float64 = 127
		var cols float64 = 7

		var lowerBound float64 = 0
		var upperBound float64 = rows

		var switchedBounds bool = false

		var row float64
		var col float64

		for _,l := range pass {
			letter := string(l)

			/**

			F means to take the lower half, keeping rows 0 through 63.
			B means to take the upper half, keeping rows 32 through 63.
			F means to take the lower half, keeping rows 32 through 47.
			B means to take the upper half, keeping rows 40 through 47.
			B keeps rows 44 through 47.
			F keeps rows 44 through 45.
			The final F keeps the lower of the two, row 44.

			R means to take the upper half, keeping columns 4 through 7.
			L means to take the lower half, keeping columns 4 through 5.
			The final R keeps the upper of the two, column 5.

			 */
			switch letter {
			case "F":
				upperBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "B":
				lowerBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "L":
				if !switchedBounds {
					row = lowerBound

					lowerBound = 0
					upperBound = cols

					switchedBounds = true
				}

				upperBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "R":
				if !switchedBounds {
					row = lowerBound

					lowerBound = 0
					upperBound = cols

					switchedBounds = true
				}

				lowerBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			}

			upperBound = math.Floor(upperBound)
			lowerBound = math.Ceil(lowerBound)
			log.Printf("letter=%s lowerBound=%.2f upperBound=%.2f switchedBounds=%v", letter, lowerBound, upperBound, switchedBounds)
		}

		col = lowerBound

		seatID = (row * 8) + col
		log.Printf("seatID=%.2f row=%.2f col=%.2f", seatID, row, col)

		if seatID > largestSeatID {
			largestSeatID = seatID
			log.Printf("largestSeatID=%.2f", largestSeatID)
		}
	}

	// at the end (<ROW> * 8) + <COLUMN> = <ANSWER>

	// sanity check; What is the highest seat ID on a boarding pass? (< 1024)
	log.Printf("largestSeatID=%.0f", largestSeatID)

	log.Print("=== Part 2 ===")

	var seatIDs []float64

	var mySeatID float64 = 0

	for _,pass := range rows {
		var seatID float64

		// zero indexed default rows
		var rows float64 = 127
		var cols float64 = 7

		var lowerBound float64 = 0
		var upperBound float64 = rows

		var switchedBounds bool = false

		var row float64
		var col float64

		for _,l := range pass {
			letter := string(l)

			/**

			F means to take the lower half, keeping rows 0 through 63.
			B means to take the upper half, keeping rows 32 through 63.
			F means to take the lower half, keeping rows 32 through 47.
			B means to take the upper half, keeping rows 40 through 47.
			B keeps rows 44 through 47.
			F keeps rows 44 through 45.
			The final F keeps the lower of the two, row 44.

			R means to take the upper half, keeping columns 4 through 7.
			L means to take the lower half, keeping columns 4 through 5.
			The final R keeps the upper of the two, column 5.

			*/
			switch letter {
			case "F":
				upperBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "B":
				lowerBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "L":
				if !switchedBounds {
					row = lowerBound

					lowerBound = 0
					upperBound = cols

					switchedBounds = true
				}

				upperBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			case "R":
				if !switchedBounds {
					row = lowerBound

					lowerBound = 0
					upperBound = cols

					switchedBounds = true
				}

				lowerBound = lowerBound + ((upperBound - lowerBound) / 2)
				break
			}

			upperBound = math.Floor(upperBound)
			lowerBound = math.Ceil(lowerBound)
			//log.Printf("letter=%s lowerBound=%.2f upperBound=%.2f switchedBounds=%v", letter, lowerBound, upperBound, switchedBounds)
		}

		col = lowerBound

		seatID = (row * 8) + col
		//log.Printf("seatID=%.2f row=%.2f col=%.2f", seatID, row, col)

		if seatID > largestSeatID {
			largestSeatID = seatID
			log.Printf("largestSeatID=%.2f", largestSeatID)
		}

		if seatID < smallestSeatID {
			smallestSeatID = seatID
			log.Printf("smallestSeatID=%.2f", smallestSeatID)
		}

		seatIDs = append(seatIDs, seatID)
	}

	var missingIDs []float64
	for i := smallestSeatID; i <= largestSeatID; i++ {
		found := false

		for _,v := range seatIDs {
			if v == i {
				found = true
				break
			}
		}

		if !found {
			missingIDs = append(missingIDs, i)
		}
	}

	for _,missingID := range missingIDs {
		log.Printf("missing=%.0f", missingID)
	}

	log.Printf("mySeatID=%.0f", mySeatID)
}
