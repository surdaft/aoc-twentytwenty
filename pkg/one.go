package main

import (
	"bufio"
	"flag"
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
)

// In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.
func main() {
	var log *logrus.Logger = logrus.New()

	// flag to provide input source
	var inputFile string = ""

	flag.StringVar(&inputFile, "input", "", "Input for task")
	flag.Parse()

	if inputFile == "" {
		log.Error("Filename must not be empty")
		return
	}

	// open file
	dat, err := os.Open(inputFile)
	if err != nil {
		log.Error(err)
		return
	}

	// iterate through items
		// if > 2020 continue
		// if min() store

	var smallest int = 2020
	var values []int

	scanner := bufio.NewScanner(dat)
	for scanner.Scan() {
		datum, _ := strconv.Atoi(scanner.Text())

		if datum > 2020 {
			continue;
		}

		if datum < smallest {
			smallest = datum
		}

		values = append(values, datum)
	}

	// if int + min > 2020 continue
	log.Printf("smallest number is %d", smallest)

	// reset scanner to the top
	var viableValues []int
	for _, i := range values {
		if (i + smallest) > 2020 {
			continue
		}

		viableValues = append(viableValues, i)
	}

	log.Printf("viable values - %d", len(viableValues))

	var matches []int

	log.Printf("=== Part 1 ===")

	// part 1
	for _, a := range viableValues {
		for _, b := range viableValues {
			//log.Printf("%d + %d = %d", a, b, a + b)
			if (a + b) == 2020 {
				log.Printf("%d + %d = 2020", a, b)
				matches = append(matches, a, b)
				break
			}
		}

		if len(matches) > 0 {
			break
		}
	}

	log.Printf("%d + %d = %d", matches[0], matches[1], matches[0] + matches[1])
	log.Printf("%d * %d = %d", matches[0], matches[1], matches[0] * matches[1])

	// part 2
	log.Printf("=== Part 2 ===")

	// reset it
	matches = []int{}

	for _, a := range viableValues {
		for _, b := range viableValues {
			for _, c := range viableValues {
				//log.Printf("%d + %d = %d", a, b, a + b)
				if (a + b + c) == 2020 {
					log.Printf("%d + %d + %d = 2020", a, b, c)
					matches = append(matches, a, b, c)
					break
				}
			}
		}

		if len(matches) > 0 {
			break
		}
	}

	log.Printf("%d + %d + %d = %d", matches[0], matches[1], matches[2], matches[0] + matches[1] + matches[2])
	log.Printf("%d * %d * %d = %d", matches[0], matches[1], matches[2], matches[0] * matches[1] * matches[2])
}
