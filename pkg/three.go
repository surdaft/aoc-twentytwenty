package main

import (
	"bufio"
	"flag"
	"github.com/sirupsen/logrus"
	"os"
)

func main() {
	var log *logrus.Logger = logrus.New()

	// flag to provide input source
	var inputFile string = ""

	flag.StringVar(&inputFile, "input", "", "Input for task")
	flag.Parse()

	if inputFile == "" {
		log.Error("Filename must not be empty")
		return
	}

	// open file
	dat, err := os.Open(inputFile)
	if err != nil {
		log.Error(err)
		return
	}

	var treesEncountered int = 0
	var rows []string

	scanner := bufio.NewScanner(dat)
	for scanner.Scan() {
		rows = append(rows, scanner.Text())
	}

	d := rows

	log.Print("=== Part 1 ===")

	// before we try and see what trees we encounter, lets build out the map
	// so we go across 3 before we go down... so it needs to be
	// 3 times the length in width

	var length int = len(rows)

	for i,row := range rows {
		for len(row) < (length * 3) {
			row += row
		}

		rows[i] = row
	}

	log.Printf("width=%d", len(rows[0]))

	// cool! now lets try this

	var x int = 0
	for i,_ := range rows {
		// ok bump across 3
		x += 3

		// we reached the bottom!
		if (i + 1) >= len(rows) {
			break
		}

		// ok, so get the next row, in the same column
		if string(rows[i + 1][x]) == "#" {
			treesEncountered++
		}
	}

	log.Printf("treesEncountered=%d", treesEncountered)


	log.Print("=== Part 2 ===")

	// before we try and see what trees we encounter, lets build out the map
	// so we go across 3 before we go down... so it needs to be
	// 3 times the length in width

	rows = d
	length = len(rows)

	traverseX := []int{
		1,
		3,
		5,
		7,
		1,
	}

	traverseY := []int{
		1,
		1,
		1,
		1,
		2,
	}

	_, largestX := findMinAndMax(traverseX)

	for i,row := range rows {
		for len(row) < (length * largestX) {
			row += row
		}

		rows[i] = row
	}

	log.Printf("width=%d", len(rows[0]))

	// cool! now lets try this

	var treesEncounteredN []int

	for i,traverseXDatum := range traverseX {
		traverseYDatum := traverseY[i]
		treesEncounteredN = append(treesEncounteredN, 0)
		x = 0
		for ii := 0; ii < len(rows); ii++ {
			// ok bump across 3
			x += traverseXDatum

			// we reached the bottom!
			if (ii + traverseYDatum) >= len(rows) {
				break
			}

			// ok, so get the next row, in the same column
			if string(rows[ii+traverseYDatum][x]) == "#" {
				treesEncounteredN[i]++
			}

			ii += traverseYDatum - 1
		}

		log.Printf("treesEncountered=%d traverseXDatum=%d traverseYDatum=%d", treesEncounteredN[i], traverseXDatum, traverseYDatum)
	}

	log.Printf(
		"%d * %d * %d * %d * %d = %d",
		treesEncounteredN[0],
		treesEncounteredN[1],
		treesEncounteredN[2],
		treesEncounteredN[3],
		treesEncounteredN[4],
		treesEncounteredN[0] * treesEncounteredN[1] * treesEncounteredN[2] * treesEncounteredN[3] * treesEncounteredN[4],
	)
}

func findMinAndMax(a []int) (min int, max int) {
	min = a[0]
	max = a[0]
	for _, value := range a {
		if value < min {
			min = value
		}
		if value > max {
			max = value
		}
	}
	return min, max
}